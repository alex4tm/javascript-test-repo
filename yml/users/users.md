## POST /users
__Create a new user__<br />Action: create<br />Parameters:
```
  name: string
  email: string
```
Response:
```
  success: 200
  error: 400
```


## PUT /users/{id}
__Update a note__<br />Action: update<br />Parameters:
```
  id: string
  name: string
  email: string
```
Response:
```
  success: 204
  error: 400
```


## DELETE /users/{id}
__Delete a note__<br />Action: delete<br />Parameters:
```
  id: string
```
Response:
```
  success: 200
  error: 400
```


